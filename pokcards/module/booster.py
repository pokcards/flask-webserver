from random import choice
from pokemontcgsdk import Card
from pokemontcgsdk import RestClient
from pokcards.module.tools import rarity_set


class Booster(object):
    """docstring for Booster."""

    def __init__(self, size, extension):
        """Initialisation de l'objet booster."""
        super().__init__()
        self.size = size
        self.extension = str(extension)
        self.rare_rate = 0.1
        self.uncommon_rate = 0.3
        self.common_rate = 0.6
        self.content = self.fill()

    def fill(self):
        """Fonction permettant de remplir un booster."""
        cartes = {}
        no_rare = round(self.size*self.rare_rate)
        no_uncommon = round(self.size*self.uncommon_rate)
        no_common = self.size-(no_rare+no_uncommon)

        RestClient.configure('12345678-1234-1234-1234-123456789ABC')
        set_cards = Card.where(q='set.name:{}'.format(self.extension))
        common, uncommon, rare = rarity_set(set_cards)

        for x in range(no_rare):
            add_carte = choice(rare)
            cartes["rare{}".format(x)] = add_carte
        for x in range(no_uncommon):
            add_carte = choice(uncommon)
            cartes["unco{}".format(x)] = add_carte
        for x in range(no_common):
            add_carte = choice(common)
            cartes["common{}".format(x)] = add_carte

        return cartes
