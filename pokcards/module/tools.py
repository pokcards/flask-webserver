def rarity_set(extension):
    """Fonction permettant de trier les cartes d'une extension par rareté."""
    common = []
    uncommon = []
    rare = []
    for carte in extension:
        if carte.rarity == "Common":
            common.append(carte)
        elif carte.rarity == "Uncommon":
            uncommon.append(carte)
        else:
            rare.append(carte)
    return common, uncommon, rare
