from pokcards import app
from flask import Flask, render_template, jsonify
from pokcards.module.booster import Booster


@app.route("/")
def helloWorld():
    return render_template('index.html', booster=Booster(10, 'Generations'))


@app.route("/get_booster", methods=['GET'])
def getBooster():
    return jsonify(Booster(10, 'Generations').content)

#  if __name__ == "__main__":
#    app.run(host='0.0.0.0')
